/**
 * Created by jvallej on 06/28/2017.
 */
'use strict';

import Vue from "vue";

const reportURL = '/penguin-report/api';

const ReportService = {
    methods: {
        get(date = undefined) {
            return this.$http.get(reportURL, {
                params: {
                    date: date
                },
                timeout: 60000,
                responseType: 'json'
            });
        }
    }
};

export default Vue.extend({
    mixins: [ReportService]
});
