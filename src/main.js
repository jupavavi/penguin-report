import Vue from 'vue'
import App from '@/App'
import plugins from '@/plugins';
import globalComponents from '@/globalComponents';

plugins(Vue); // Init plugins
globalComponents(Vue); // Init global components

Vue.config.productionTip = false

new Vue({
  el: '#app',
  render: h => h(App)
});
