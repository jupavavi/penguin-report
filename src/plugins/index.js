import VueResource from 'vue-resource';

export default function(Vue) {
    Vue.use(VueResource);
};
