import Clock from './Clock';
import Hero from './Hero';
import Modal from './Modal';

export default function(Vue) {
    // Global components
    Vue.component('Clock', Clock);
    Vue.component('Hero', Hero);
    Vue.component('Modal', Modal);
};
