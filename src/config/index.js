export default {
  minReportHours: 7,
  reportHourLimit: 9.5 // 9:30 am
};